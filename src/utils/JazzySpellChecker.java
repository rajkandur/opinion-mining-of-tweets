package utils;
import com.swabunga.spell.engine.SpellDictionaryHashMap;
import com.swabunga.spell.event.SpellChecker;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class JazzySpellChecker {

	protected static SpellDictionaryHashMap dictionary = null;
	protected static SpellChecker spellChecker = null;

	static {

		try {

			dictionary =
				new SpellDictionaryHashMap(new
						File("lib/jazzy/dictionary/eng_com.dic"));

			dictionary.addDictionary(new
					File("lib/jazzy/dictionary/center.dic"));

			dictionary.addDictionary(new
					File("lib/jazzy/dictionary/color.dic"));

			dictionary.addDictionary(new
					File("lib/jazzy/dictionary/ize.dic"));

			dictionary.addDictionary(new
					File("lib/jazzy/dictionary/labeled.dic"));

			dictionary.addDictionary(new
					File("lib/jazzy/dictionary/yze.dic"));


			spellChecker = new SpellChecker(dictionary);

		}
		catch (IOException e) {

			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static String getSuggestions(String word, int threshold) {

		String suggestionWord = new String();
		List<String> sugglist = spellChecker.getSuggestions(word, threshold);
		if ( null != sugglist ){
			for (int i = 0; i < sugglist.size(); i++) {
				if(word.length() <= sugglist.get(i).length() ){
					suggestionWord =  sugglist.get(i);
					break;
				}
			}
		}
		
		//return the first word if suggestionWord is empty
		if(suggestionWord.isEmpty())
			suggestionWord = sugglist.get(0);
		
		return suggestionWord;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub


		String suggestionWd = getSuggestions ("hapy", 5); // Pass each word from the tweet
		//for (int i = 0; i < listSuggestions.size(); i++)
		if ( null != suggestionWd )
			System.out.println(suggestionWd);
		else
			System.out.println("No Suggestions");

	}

}




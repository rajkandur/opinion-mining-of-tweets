package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.map.MultiValueMap;

import parser.StanfordWrapParser;

public class Utilities {

	public enum SENTIMENT {POSITIVE, NEGATIVE, NEUTRAL};
	public enum NGRAM {UNIGRAM, BIGRAM, TRIGRAM, FOURGRAM}; 
	private String m_emoticon_string;
	private String m_phone_string;
	private List<String> m_http_string;
	private String m_username_string;
	private String m_hashtag_string;
	
	public Utilities() {
		
		//if you need only emoticons
		m_emoticon_string =
		      "([<>]" +
		      "[:;=8]" +                     // eyes
		      "[\\-o\\*\']" +                // optional nose
		      "[\\)\\]\\(\\[dDpP/\\:\\}\\{@\\|\\\\]" + // mouth      
		      "|" +
		      "[\\)\\]\\(\\[dDpP/\\:\\}\\{@\\|\\\\]" + // mouth
		      "[\\-o\\*\']" +                 // optional nose
		      "[:;=8]" +                     // eyes
		      "[<>])";
		
		// Phone numbers:
		m_phone_string = 
	    "(" +
	      "(" +            // (international)
	        "\\+?[01]" +
	        "[\\-\\s.]*" +
	      ")?" +            
	      "(" +           // (area code)
	        "[\\(]?" +
	        "\\d{3}" +
	        "[\\-\\s.\\)]*" +
	      ")?" +    
	      "\\d{3}" +          // exchange
	      "[\\-\\s.]*" +   
	      "\\d{4}" +          // base
	    ")";
	      
	    //url
		m_http_string = Arrays.asList("(http[s]?://[\\w]*[/]*\\.[a-zA-Z]{3,3}[ \\.]?)", "http[s]?://t.co/........"); 
		
		// Twitter username:
	    m_username_string = "@[\\w_]+";
	    
	    // Twitter hashtags:
	    m_hashtag_string = "\\#+[\\w_]+[\\w\'_\\-]*[\\w_]+";			
	}
	
	public enum TOKENTYPE{
		EMOTICON,
		HTTP,
		PHONE,
		USERNAME
	}
	
	/**
	 * Construct Ngrams for the given sentence and returns as list
	 * @param sentence
	 * @param engram
	 * @return
	 */
	public List<String> constructNGrams(List<String> srcTokens, NGRAM engram){

//		sentence = sentence.replaceAll("[,|;|\\-|.|\"]", "");
//		sentence = sentence.replaceAll("  *", " ");
//		String[] src = sentence.split(" ");
		List<String> ngramList = new ArrayList<String>();
		
		String tempf = null;
		String temps = null;
		String tempt = null;
		switch (engram) {
		case UNIGRAM:
			ngramList = srcTokens;
			break;
		case BIGRAM:
			for (int i = 0; i < srcTokens.size()-1; i++) {
				//check for null
				if(srcTokens.get(i) == null || srcTokens.get(i+1) == null)
					continue;
				
				String first = srcTokens.get(i).trim();
				String second = srcTokens.get(i+1).trim();

				//bracket entities should be done separately
				if(first.contentEquals("(") || second.contentEquals(")")) continue;
				else if(second.contentEquals("(")){
					tempf = first;
					continue;
				}	
				else if(first.contentEquals(")")){
					first = tempf;
					tempf = "";
				}
				ngramList.add(first + " " + second);
			}
			break;
		case TRIGRAM:
			for (int i = 0; i < srcTokens.size()-2; i++) {
				
				if(srcTokens.get(i) == null || srcTokens.get(i+1) == null || srcTokens.get(i+2) == null)
					continue;
				
				String first = srcTokens.get(i).trim();
				String second = srcTokens.get(i+1).trim();
				String third = srcTokens.get(i+2).trim();
				//bracket entities should be done separately
				if( first.contentEquals("(") || second.contentEquals("(") ||  
						second.contentEquals(")") || third.contentEquals(")")) continue;
				else if(third.contentEquals("(")){
					tempf = first;
					temps = second;
					continue;
				}	
				else if(first.contentEquals(")")){
					ngramList.add(tempf + " " + temps + " " + second);
					first = temps;
					tempf = temps = "";
				}
				ngramList.add(first + " " + second + " " + third);
			}
			break;
		case FOURGRAM:
			for (int i = 0; i < srcTokens.size()-3; i++) {
				
				if(srcTokens.get(i) == null || srcTokens.get(i+1) == null || 
						srcTokens.get(i+2) == null || srcTokens.get(i+3) == null)
					continue;
				
				String first = srcTokens.get(i).trim();
				String second = srcTokens.get(i+1).trim();
				String third = srcTokens.get(i+2).trim();
				String fourth = srcTokens.get(i+3).trim();
				//bracket entities should be done separately
				if( first.contentEquals("(") || second.contentEquals("(") || third.contentEquals("(") || 
						second.contentEquals(")") || third.contentEquals(")") || fourth.contentEquals(")")) continue;
				else if(fourth.contentEquals("(")){
					tempf = first;
					temps = second;
					tempt = third;
					continue;
				}	
				else if(first.contentEquals(")")){
					ngramList.add(tempf + " " + temps + " " + tempt + " " + second);
					ngramList.add(temps + " " + tempt + " " + second + " " + third);
					first = tempt;
					tempf = temps = tempt = "";
				}
				ngramList.add(first + " " + second + " " + third + " " + fourth);
			}
			break;
		default:
			return null;
		}
		return ngramList;
	}
	
	public List<List<String[]>> createRandomParition(float trainpercent, List<String[]> wholedata) {
		
		List<List<String[]>> traintestSplit = new ArrayList<List<String[]>>();
		
		//randomize the data: uncomment for cross-validation
		//Collections.shuffle(wholedata);
		
		//Create the partition
		int trainTweetsCount =  Math.round(wholedata.size() * trainpercent);
		List<String[]> train = new ArrayList<String[]>();
		List<String[]> test = new ArrayList<String[]>();
		for(int i = 0; i < wholedata.size(); i++){
			if(i < trainTweetsCount)
				train.add(wholedata.get(i));//train data
			else
				test.add(wholedata.get(i));//test data
		}
		
		//add to the split
		traintestSplit.add(train);
		traintestSplit.add(test);
		return traintestSplit;
	}
	
	@SuppressWarnings("unchecked")
	public String cleanData(String tweet){
		
		//trim
		tweet = tweet.trim();
		
		//remove usernames
		tweet = tweet.replaceAll("@+[a-zA-z0-9]*[ |\r\n]", ""); //@+[\w_]+

		//remove links
		tweet = tweet.replaceAll("http://t.co/........", "");

		//remove entities
		StanfordWrapParser parser = StanfordWrapParser.getInstance();
		MultiValueMap NEmap =  parser.findNamedEntities(tweet);
		Set<String> keys =  NEmap.keySet();
		for(String key: keys) {
			Collection<List<String>> valueList =  NEmap.getCollection(key); 
			for (List<String> values : valueList) {
				for (String value : values) 
					tweet = tweet.replaceAll(value, "");
			}
			
			//also remove obama, barack, romney, mitt
			tweet = tweet.replaceAll("(([oO][bB][aA][mM][aA])|([bB][aA][rR][aA][cC][kK])|" +
					         "([rR][oO][mM][nN][eE][yYiE])|([mM][iI][tT][tT]))", "");
		}

		//System.out.println(tweet);
		return tweet;
	}
	/**
	 * Reference: http://sentiment.christopherpotts.net/code-data/happyfuntokenizing.py
	 * @param sentence
	 * @return
	 */
	public List<String> tweetTokenizer(String sentence){
		
		List<String> tokenizedSentence = new ArrayList<String>();
				
		// The components of the tokenizer: order is important
		List<String> regex_strings = Arrays.asList( 
		    m_phone_string
		    ,
		    // Emoticons:
		    m_emoticon_string
		    ,
		    //http format 1
		    m_http_string.get(0)
		    
		    , //http format 2
		    m_http_string.get(1)
		    ,    
		    // HTML tags:
		     "<[^>]+>"
		    ,
		    m_username_string
		    ,
		    m_hashtag_string
		    ,
		    // Remaining word types:
		    "([a-zA-Z]+['\\-_][a-zA-Z])"      // Words with apostrophes or dashes.
		    ,
		    "([+\\-]?\\d+[,/.:-]\\d+[+\\-]?)"  // Numbers, including fractions, decimals.
		    ,
		    "([\\w_]+)"                    // Words without apostrophes or dashes.
		    ,
		    "(\\.(?:\\s*\\.){1,})"           // Ellipsis dots. 
		    ,
		    "(\\S)"                   // Everything else that isn't whitespace.
		    );
		
		//order of patterns is important: Taking only first best entire match.
		for (String token : sentence.split(" ")) {
			for (String regexpat : regex_strings) {
				Pattern pattern = Pattern.compile("(" + regexpat + ")");
				Matcher matcher = pattern.matcher(token);
				if(matcher.find()){
					String c = matcher.group();
					if(!c.isEmpty()){
						tokenizedSentence.add(c);
						break;
					}
				}
			}
		}
		
		//System.out.println(tokenizedSentence);
		return tokenizedSentence;
	}
	
	/**
	 * Returns the index of the TOKENTYPE present in the input
	 * @param text
	 * @return
	 */
	public List<Integer> findTokenIndex(List<String> text, TOKENTYPE eTokenType){
		
		List<Integer> indexes = new ArrayList<Integer>();
				
		List<String> regexlist = new ArrayList<String>();
		switch(eTokenType){
		case EMOTICON:
			regexlist.add(m_emoticon_string);
			break;
		case PHONE:
			regexlist.add(m_phone_string);
			break;
		case HTTP:
			regexlist.addAll(m_http_string);
			break;
		case USERNAME:
			break;
		}
		
		//find the index
		for (String regex : regexlist) {
			for (int i = 0; i < text.size(); i++) {			
				Pattern pattern = Pattern.compile("(" + regex + ")");
				Matcher matcher = pattern.matcher(text.get(i));
				if(matcher.find()){
					String c = matcher.group();
					if(!c.isEmpty())
						indexes.add(i);
				}
			}
		}
		
		//System.out.println(indexes);
		return indexes;
	}
	
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Utilities utils = new Utilities();
		String tweet = " @SentimentSymp: can't wait for the Nov 9 (224)419-1499    I'm process          #Sentiment talks! YAAAAAAY!!! >:-D http://sentimentsymposium.com/.";
		//System.out.println(utils.constructNGrams(tweet, NGRAM.UNIGRAM));
		List<String> tokens = utils.tweetTokenizer(tweet);
		utils.findTokenIndex(tokens, TOKENTYPE.EMOTICON);
	}
}

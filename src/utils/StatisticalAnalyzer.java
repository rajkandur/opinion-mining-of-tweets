package utils;

public class StatisticalAnalyzer {
	
	/**
	 * 
	 * @param w1w2
	 * @param w1
	 * @param w2
	 * @param n
	 * @return: PMI = log2(C(w1,w2)/N / (C(w1)/N * C(w2)/N))
	 */
	public double PMI(int w1w2, int w1, int w2, int n){	
		
		System.out.println("ph1ph2 count: " + w1w2);
		System.out.println("ph1 count: " + w1);
		System.out.println("ph2 count: " + w2);
		System.out.println("n count: " + n);
		
		double pmi = 0.0;
		double nDependent =  w1w2 / (double)n;
		double nIndependent = (w1/(double)n) * (w2/(double)n);
		
		//calculate using change of bases
		pmi = Math.log(nDependent / nIndependent) / (double) Math.log(2);
				
		//return pmi;
		return Double.parseDouble(String.format("%.2f", pmi));
	}
	
	/**	
	 * 
	 * @param w1w2
	 * @param w1notw2
	 * @param notw1w2
	 * @param notw1notw2
	 * @return: chi2 = (abs(Oij - Eij)-0.5)2 / Eij : if the degrees of freedom is 1, use Yates correction of continuity
	 * http://www.okstate.edu/ag/agedcm4h/academic/aged5980a/5980/newpage28.htm
	 */
	public double chisquare(int w1w2, int w1notw2, int notw1w2, int notw1notw2){
		
		System.out.println("ph1ph2 count: " + w1w2);
		System.out.println("ph1~ph2 count: " + w1notw2);
		System.out.println("~ph1ph2 count: " + notw1w2);
		System.out.println("~ph1~ph2 count: " + notw1notw2);
		
		double Ow1w2 = w1w2;
		double Ew1w2 = ((w1w2+w1notw2) * (w1w2+notw1w2))/(double)(w1w2+w1notw2+notw1w2+notw1notw2);
		double num = Math.pow(Math.abs(Ow1w2 - Ew1w2)-0.5, 2);
		double den = Ew1w2;
		
		double chisquare = num / (double)den;
		return Double.parseDouble(String.format("%.2f", chisquare));
	}
	
	/**
	 * Dunning '93 and Anoop Kunchukuttan Stage Presentation
	 * null hypothesis: p(w2|w1) = p(w2|~w1)
	 * alternate hypothesis: p(w2|w1) != p(w2|~w1)
	 * @param w1w2
	 * @param w1notw2
	 * @param notw1w2
	 * @param notw1notw2
	 * @return: log likelihood ratio: -2.log.lamda = 2[log L(pl, kl, n1) + logL(p2, k2, n2) - logL(p, kl, nl) - logL(p, k2, n2)]
	 *          where logL(p,n,k) = k log(p) + (n-k)log(1-p)
	 */
	public double loglikelihoodratio(int w1w2, int w1notw2, int notw1w2, int notw1notw2){

//		The following are the quantities involved
//		p1 = P(w2|w1), p2 = P(w2|~w1) , n1 = c1, k1 = c12
//		n2 = n - c1, k2 = c2 - c12
//		c1, c2, c12 =corpus frequencies of w1,w2,w1w2
//		n=total number of words in the corpus
//		For the alternate hypothesis, the MLE estimates of p1, p2 are,
//		p1 =k1/n1 and p2 =k2/n2
//		For the null hypothesis, we have p1 = p2 = p.
//		p =(k1 + k2)/(n1 + n2)
		
		System.out.println("ph1ph2 count: " + w1w2);
		System.out.println("ph1~ph2 count: " + w1notw2);
		System.out.println("~ph1ph2 count: " + notw1w2);
		System.out.println("~ph1~ph2 count: " + notw1notw2);
		
		//corpus counts
		double n1 = w1w2 + w1notw2;
		double k1 = w1w2;
		double n = w1w2 + w1notw2 + notw1w2 + notw1notw2;
		double n2 = n - n1;
		double k2 = (w1w2 + notw1w2) - w1w2;
		
		//alternate hypothesis
		double p1 = k1 / (double)n1;
		double p2 = k2 / (double)n2;
		
		//null hypothesis
		double p = (k1 + k2) /(n1 + n2);
		
		//loglikelihood constituents
		double logLp1k1n1 = (k1*Math.log(p1)) + ((n1-k1) * Math.log(1-p1));
		double logLp2k2n2 = (k2*Math.log(p2)) + ((n2-k2) * Math.log(1-p2));
		double logLpk1n1 = (k1*Math.log(p)) + ((n1-k1) * Math.log(1-p));
		double logLpk2n2 = (k2*Math.log(p)) + ((n2-k2) * Math.log(1-p));
		
		// LogLikelihood ratio
		double logLikelihood = 2 * (logLp1k1n1 + logLp2k2n2 - logLpk1n1 - logLpk2n2);
		Double value = Double.parseDouble(String.format("%.2f", logLikelihood));
		
		//Co-occurance is very strong. And the constituents are not appearing separately
		if(value.isNaN() && p1 == 1.0)
			value = 10000.00;
				
		return value;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		StatisticalAnalyzer analyzer =  new StatisticalAnalyzer();
		
		//check PMI SNLP page 179
		double pmi = analyzer.PMI(20, (42), (20), 14307668);
		System.out.println(pmi);
		
		//check chi-square // SNLP page 179
		double chisquare = analyzer.chisquare(31950, 12004, 4793, 848330);
		System.out.println(chisquare);
		
		//check loglikelihood //dunning 93 bigram example for "the swiss"
		double log = analyzer.loglikelihoodratio(110, 2442, 111, 29114);
		System.out.println(log);
	}
}

package jswordnet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import parser.OpenNLPWrapParser;
import simpack.util.corpus.Stopwords;
import simpack.util.corpus.StringUtils;
import edu.smu.tspell.wordnet.AdjectiveSynset;
import edu.smu.tspell.wordnet.AdverbSynset;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.VerbSynset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;

public class WordNetUtils {

	private WordNetDatabase wordNet;
	private PorterStemmer m_PStemmer; // just a upper level class for underlying Standard Stemmer Algorithm
	OpenNLPWrapParser m_Parser;
	HashMap<String, String> m_hashRepeatedStr;
	private static WordNetUtils m_WNUtils = null;
	
	public static WordNetUtils getInstance(){
		if(null == m_WNUtils)
			m_WNUtils = new WordNetUtils();
		return m_WNUtils;
	}
	
	private WordNetUtils(){
		initWordnet();

		m_PStemmer = new PorterStemmer();
		m_Parser = OpenNLPWrapParser.getInstance();
		m_hashRepeatedStr = new HashMap<String, String>();
	}

	private void initWordnet() {
		// Initialize the WordNet interface.
		String cwd = System.getProperty("user.dir");
		String wordnetdir = cwd + "/db/WordNetdb-3.0";
		System.setProperty("wordnet.database.dir", wordnetdir);
		// Instantiate 
		try {
			wordNet = WordNetDatabase.getFileInstance();
		}
		catch( Exception e ) {
			e.printStackTrace();
		}
	}

	public void initMatchingHash()
	{
		m_hashRepeatedStr.clear();
	}

	public boolean bAreSynonyms(String source, String target, SynsetType type) {

		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);
		
		return bCompareTwoWordsPhrases( sourceSynsets, targetSynsets, type);
	}

	public boolean bAreinstanceHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) targetSynsets[i]).getInstanceHypernyms();
				if( bCompareTwoWordsPhrases( srcSynsets, hypernymNounSynset, type ) )
					return true;
			}
		}
		return false;
	}

	public boolean bAreHypernyms(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		VerbSynset[] hypernymVerbSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset) targetSynsets[i]).getHypernyms();
				if( bCompareTwoWordsPhrases( srcSynsets, hypernymNounSynset, type) )
					return true;
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < targetSynsets.length; i++) {
				hypernymVerbSynset = ((VerbSynset)targetSynsets[i]).getHypernyms();
				if( bCompareTwoWordsPhrases(srcSynsets, hypernymVerbSynset, type ))
					return true;
			}
		}

		return false;
	}

	public boolean bAreinstanceHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hyponymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < targetSynsets.length; i++) {
				hyponymNounSynset = ((NounSynset) targetSynsets[i]).getInstanceHyponyms();
				if( bCompareTwoWordsPhrases( srcSynsets, hyponymNounSynset, type) )
					return true;
			}
		}

		return false;
	}

	public boolean bAreHyponyms(String source, String target, SynsetType type) {

		NounSynset[] hyponymNounSynset;
		Synset[] srcSynsets = wordNet.getSynsets(source, type );
		Synset[] targetSynsets = wordNet.getSynsets(target, type );

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < srcSynsets.length; i++) {
				hyponymNounSynset = ((NounSynset) srcSynsets[i]).getHyponyms();
				if( bCompareTwoWordsPhrases( hyponymNounSynset,targetSynsets, type) )
					return true;
			}
		}

		return false;
	}

	public boolean bArePertainyms(String source, String target){
		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.NOUN);

		for (int i = 0; i < sourceSynsets.length; i++) {
			AdjectiveSynset[] adjSynset = ((NounSynset)sourceSynsets[i]).getAttributes();
			for(int j = 0; j < adjSynset.length; j++){
				WordSense[] relatedSensewords = adjSynset[j].getPertainyms(target);
				for(int k = 0; k < relatedSensewords.length; k++)
					if( bCompareTwoWordsPhrases( sourceSynsets[i], relatedSensewords[k].getSynset(), SynsetType.NOUN))
						return true;
			}
		}		

		return false;
	}

	public boolean bAreTopicMembers(String source, String target){

		Synset[] sourceSynsets = wordNet.getSynsets(source, SynsetType.NOUN );
		Synset[] targetSynsets = wordNet.getSynsets(target, SynsetType.NOUN);

		for (int i = 0; i < sourceSynsets.length; i++) {
			Synset[] topicMembersSynset = ((NounSynset)sourceSynsets[i]).getTopicMembers();
			if( bCompareTwoWordsPhrases( topicMembersSynset, targetSynsets, SynsetType.NOUN) )
				return true;
		}

		return false;
	}

	public boolean bAreOfTopics(String source, String target, SynsetType type) {

		NounSynset[] hypernymNounSynset;
		Synset[] sourceSynsets = wordNet.getSynsets(source, type);
		Synset[] targetSynsets = wordNet.getSynsets(target, type);

		//selectively choose the synset depending on the type
		if(type == SynsetType.NOUN ){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((NounSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoWordsPhrases( hypernymNounSynset, targetSynsets, type ))
					return true;
			}
		}
		else if(type == SynsetType.VERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((VerbSynset)sourceSynsets[i]).getTopics();
				if(bCompareTwoWordsPhrases( hypernymNounSynset, targetSynsets, type ))
					return true;
			}
		}
		else if(type == SynsetType.ADJECTIVE){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdjectiveSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoWordsPhrases( hypernymNounSynset, targetSynsets, type ))
					return true;
			}
		}
		else if(type == SynsetType.ADVERB){
			for (int i = 0; i < sourceSynsets.length; i++) {
				hypernymNounSynset = ((AdverbSynset)sourceSynsets[i]).getTopics();
				if( bCompareTwoWordsPhrases( hypernymNounSynset, targetSynsets, type ))
					return true;
			}
		}

		return false;
	}

	boolean bCompareTwoWordsPhrases(Synset[] srcSynset, Synset[] tarSynset, SynsetType type){

		for (int j = 0; j < srcSynset.length; j++) {
			for (int k = 0; k < tarSynset.length; k++) {
				if( bCompareTwoWordsPhrases( srcSynset[j], tarSynset[k], type ) )
					return true;
			}
		}

		return false;
	}

	boolean bCompareTwoWordsPhrases(Synset srcSynset, Synset tarSynset, SynsetType type){

		String[] srcArray = srcSynset.getWordForms();
		String[] tarArray = tarSynset.getWordForms();
		String[] srcWFArray = null;
		String[] tarWFArray = null;

		//compare the definitions
		String srcDef = srcSynset.getDefinition();       
		String tarDef = tarSynset.getDefinition();
		String[] srcDefArray = null;
		String[] tarDefArray = null;
		try {

			//remove stopwords and 
			srcWFArray = removeNoise(srcArray).toString().split(" ");
			tarWFArray = removeNoise(tarArray).toString().split(" ");

			//compare word forms
			if(bCompareTwoStringArrays(srcWFArray, tarWFArray))
				return true;

			//first remove stopwords and split the definitions and find if there are any common words
			srcDefArray = removeNoise(srcDef).toString().split(" ");
			tarDefArray = removeNoise(tarDef).toString().split(" ");

		} catch (IOException e) {
			e.printStackTrace();
		}

		//compare word definitions
		if(bCompareTwoStringArrays(srcDefArray, tarDefArray))
			return true;

		return false; // else
	}

	boolean bCompareTwoStringArrays(String[] srcArray, String[] tarArray){
		for(int i = 0; i < srcArray.length; i++){
			for(int j = 0; j < tarArray.length; j++){

				//stem the strings before comparing
				String srcStem = srcArray[i].toLowerCase().replaceAll("[\\[,\\]]", " ").trim();
				srcStem = m_PStemmer.stem(srcStem);

				String tarStem = tarArray[j].toLowerCase().replaceAll("[\\[,\\]]", " ").trim();
				tarStem = m_PStemmer.stem(tarStem);

				if(m_hashRepeatedStr.containsKey(srcStem) || m_hashRepeatedStr.containsValue(tarStem))
					return true; 

				if( srcStem.equalsIgnoreCase(tarStem))
				{	m_hashRepeatedStr.put(srcStem, tarStem);
				return true;
				}

			}
		}
		return false;
	}

	public List<String> removeNoise(String sSrcString[]) throws IOException {

		List<String> trimmedList = new ArrayList<String>();
		for(int i = 0; i< sSrcString.length; i++){
			String trimmedStr = removeNoise(sSrcString[i]);
			trimmedList.add(trimmedStr);
		}
		return trimmedList;
	}

	public String removeNoise(String sSrcString) throws IOException {

		if(sSrcString.isEmpty())
			return sSrcString;
		
		//String trimmedStr = Stopwords.remove(sSrcString);
		String trimmedStr = sSrcString;

//		trimmedStr = trimmedStr.replaceAll("_","")
//		.replaceAll("-","")
//		.replaceAll("//.","");
//		.replaceAll("]","")
//		.replaceAll("[","")
//		.replaceAll("(","")
//		.replaceAll(")","");

		//removes punctuations, stopwords and stems the words
		trimmedStr = StringUtils.clean(trimmedStr);		
		return trimmedStr;
	}
}
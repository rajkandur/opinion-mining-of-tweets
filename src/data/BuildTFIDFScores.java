package data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.stanford.nlp.util.StringUtils;

import utils.Utilities;
import utils.Utilities.NGRAM;

/**
 * If you change the class structure or its member variable values delete 'tfidf.ser' and recreate it
 * @author kandur
 *
 */
public class BuildTFIDFScores implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	Map<String, Map<String, Float>> m_featTF;
	Map<String, Map<String, Float>> m_featTFIDF;
	Map<String, Map<String, Float>> m_featIDF;
	Map<String, Integer> m_allTokensMapIndex;
	private BuildTFIDFScores m_tfidfInstance = null;
	
//	/**
//	 * Singleton
//	 * @return
//	 */
//	public BuildTFIDFScores getInstance(){
//		if(null == m_tfidfInstance)
//			m_tfidfInstance = new BuildTFIDFScores();
//		
//		return m_tfidfInstance;
//	}
		
	
	public BuildTFIDFScores() {
		
		//all tokens
		m_allTokensMapIndex = new LinkedHashMap<String, Integer>();
		
		//get the representation score for every token for each class
		m_featTF =  new LinkedHashMap<String, Map<String, Float>>();
		m_featTFIDF = new LinkedHashMap<String, Map<String, Float>>();
		m_featIDF = new LinkedHashMap<String, Map<String, Float>>();
	}
	
	public void init(){
		try {
			constructTFIDFfeature();
			generateAllTokensIndexMap();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Map<String, Map<String, Float>> getM_featIDF() {
		return m_featIDF;
	}
	
	public float getIDFScore(String token, String classKey){
		
		float idf = 0.0f;
		if(m_featIDF.containsKey(token)){
			Map<String, Float> idfClassMap = m_featIDF.get(token);
			if(idfClassMap.containsKey(classKey)){
				idf = idfClassMap.get(classKey).floatValue();
			}
		}
		
		return idf;
	}
	
	public Map<String, Map<String, Float>> getM_featTFIDF() {
		return m_featTFIDF;
	}
	
	public float getTFIDFScore(String token, String classKey){

		float tfidf = 0.0f;
		if(m_featTFIDF.containsKey(token)){
			Map<String, Float> tfidfClassMap = m_featTFIDF.get(token);
			if(tfidfClassMap.containsKey(classKey)){
				tfidf = tfidfClassMap.get(classKey).floatValue();
			}
		}

		return tfidf;
	}
	
	public Map<String, Map<String, Float>> getM_featTF() {
		return m_featTF;
	}
	
	public float getTFScore(String token, String classKey){

		float tf = 0.0f;
		if(m_featTF.containsKey(token)){
			Map<String, Float> tfClassMap = m_featTF.get(token);
			if(tfClassMap.containsKey(classKey)){
				tf = tfClassMap.get(classKey).floatValue();
			}
		}

		return tf;
	}
	
	public Map<String, Integer> getM_allTokensMapIndex() {
		return m_allTokensMapIndex;
	}
	
	/**
	 * 1-indexed map: specifically made to generate feature vector for svmlight
	 */
	private void generateAllTokensIndexMap(){
		
		TweetTuple tupleInstance = TweetTuple.getInstance();
		Utilities utils = new Utilities();
		List<String> classValues = new ArrayList<String>();
		classValues.addAll(tupleInstance.getMapTrainTweets().keySet());
		int count = 1;
		for (String tkey : classValues) {
			//add all tokens
			Collection<String> tweets = tupleInstance.getMapTrainTweets().getCollection(tkey);	
			tweets.addAll(tupleInstance.getMapTestTweets().getCollection(tkey));
			
			for (String t : tweets) {
				//tokenize and collect the class tweets
				List<String> tokenslist = utils.tweetTokenizer(t);
				tokenslist = utils.constructNGrams(tokenslist, NGRAM.UNIGRAM);
				tokenslist.addAll(utils.constructNGrams(tokenslist, NGRAM.BIGRAM));
				for (String token : tokenslist) {
					
					//do not add punctuation
					boolean bContinue = false;
					for(String tok: token.split(" ")){
						if( StringUtils.isPunct(tok.trim()) || StringUtils.isNumeric(tok.trim()) ||
								tok.contains("+") || tok.contains("?")){
							bContinue = true;	
							break;
						}
					}

					//if true do not add tokens
					if(bContinue)
						continue;
					
					token = token.toLowerCase();
					if(!m_allTokensMapIndex.containsKey(token)) //add it only if it is not present
						m_allTokensMapIndex.put(token, count++);	
				}	
			}
		}
	}
	
	/**
	 * Currently using tf-idf score
	 * @param fClass
	 * @throws IOException 
	 */
	@SuppressWarnings("unchecked")
	private void constructTFIDFfeature() throws IOException{
		
		evaluateScores(NGRAM.UNIGRAM);
		evaluateScores(NGRAM.BIGRAM);
	}

	private void evaluateScores(NGRAM engram) {
		TweetTuple tupleInstance = TweetTuple.getInstance();
		Utilities utils = new Utilities();
		Set<String> allTokens = new HashSet<String>();
		List<String> classValues = new ArrayList<String>();
		classValues.addAll(tupleInstance.getMapTrainTweets().keySet());
		
		for (String tkey : classValues) {
			Collection<String> tweets = tupleInstance.getMapTrainTweets().getCollection(tkey);
			System.out.println("Train class " + tkey + ": " + tweets.size());
			for (String t : tweets) {
				
				//tokenize and collect the class tweets
				List<String> tokenslist = utils.tweetTokenizer(t);
				
				//construct ngrams
				List<String> ngramtokenlist = utils.constructNGrams(tokenslist, engram);
				for (String token : ngramtokenlist) {
					token = token.toLowerCase();

					//do not add punctuation
					boolean bContinue = false;
					for(String tok: token.split(" ")){
						if(StringUtils.isPunct(tok.trim()) || StringUtils.isNumeric(tok.trim()) || 
								tok.contains("+") || tok.contains("?") ){
							bContinue = true;	
							break;
						}
					}

					//if true, do not add tokens
					if(bContinue)
						continue;
					
					allTokens.add(token);	//unique tokens
				}	
				
			}
		}
		
		System.out.println(allTokens);
			
		//build scores every token
		IndexerSearcher indexSearcher = IndexerSearcher.getInstance(); 
		for (String token : allTokens) {
			
			Map<String, Float> tfClassMap = new HashMap<String, Float>();
			Map<String, Float> tfidfClassMap = new HashMap<String, Float>();
			Map<String, Float> idfClassMap = new HashMap<String, Float>();
			
			Float[] tf = new Float[4];
			Float[] tfidf = new Float[4];
			Float[] idf = new Float[4];
			int index = 0;
			for (String c : classValues) {
				/* tf, df and idf in order */
				List<Float> scores = indexSearcher.searchCorpus("Tweet", token, c);
				tf[index] = scores.get(0);
				tfidf[index] = scores.get(0) * scores.get(2);
				idf[index] = scores.get(2);
				index++;
				
				//only on last iteration
				if(index == 4){
					//do normalize scores
					tf = doNormalizeScores(tf);
					tfidf = doNormalizeScores(tfidf);
					idf = doNormalizeScores(idf);
					int i = 0;
					for (String c1 : classValues) {
						tfClassMap.put(c1, tf[i]);
						tfidfClassMap.put(c1, tfidf[i]);
						idfClassMap.put(c1, idf[i]);
						i++;
					}
				}
			}
			
			//add to the map
			m_featTF.put(token, tfClassMap);
			m_featTFIDF.put(token, tfidfClassMap);
			m_featIDF.put(token, idfClassMap);
		}
	}
	
	/**
	 * distribute from 0 to 1
	 * @param irScores
	 * @return
	 */
	private Float[] doNormalizeScores(Float[] irScores){
		Float[] normScores = new Float[irScores.length];
		
//		//weighted average
//		Map<String, Float> clirmap = new HashMap<String, Float>();
//		clirmap.put("0", irScores[0]);
//		clirmap.put("1", irScores[1]);
//		clirmap.put("2", irScores[2]);
//		clirmap.put("3", irScores[3]);
//		
//		//sort
//		Collections.sort(Arrays.asList(irScores));
//		
//		//assign weigths: 0.5, 0.3, 0.1, 0.1
//		Float[] wirscores = new Float[4];
//		
//		for (int i = 0; i < irScores.length; i++) {
//			
//		}
		
		
		for (int i = 0; i < normScores.length; i++) {
			normScores[i] = irScores[i] / (irScores[0] + irScores[1] + irScores[2] + irScores[3]); 
		}
		
		return normScores;
	}
}

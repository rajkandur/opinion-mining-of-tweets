package data;

import java.awt.TextField;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Analyzer.TokenStreamComponents;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.FieldInfo.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopDocsCollector;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import parser.StanfordWrapParser;
import utils.Utilities;
import edu.stanford.nlp.util.StringUtils;

public class IndexerSearcher {

	/*
	 * replace $ by obama or romney accordingly
	 */
	public String INDEXPOSDIR = "./db/index/name/pos";
	public String INDEXNEGDIR = "./db/index/name/neg";
	public String INDEXNEUDIR = "./db/index/name/neu";
	public String INDEXMIXDIR = "./db/index/name/mix";

	TweetTuple m_TweetTupleInstance = null;
	private Utilities m_Utils;
	//IndexSearcher m_IndexSearcher;	
	private static IndexerSearcher m_ISInstance = null;
		
	public enum INDEXTYPE{
		OBAMA,
		ROMNEY
	};
	
	private INDEXTYPE m_eIndexType;

	public static IndexerSearcher getInstance(){
		if(null == m_ISInstance)
			m_ISInstance = new IndexerSearcher();

		return m_ISInstance;
	}

	private IndexerSearcher() {
		m_TweetTupleInstance = TweetTuple.getInstance();
		m_Utils = new Utilities();
		//m_IndexSearcher = null;
	}
	
	public void setIndexType(INDEXTYPE eIndType){
		m_eIndexType = eIndType;
		switch(eIndType){
		case OBAMA:
			INDEXMIXDIR = INDEXMIXDIR.replaceAll("name", "obama");
			INDEXNEUDIR = INDEXNEUDIR.replaceAll("name", "obama");
			INDEXPOSDIR = INDEXPOSDIR.replaceAll("name", "obama");
			INDEXNEGDIR = INDEXNEGDIR.replaceAll("name", "obama");
			break;
		case ROMNEY:
			INDEXMIXDIR = INDEXMIXDIR.replaceAll("name", "romney");
			INDEXNEUDIR = INDEXNEUDIR.replaceAll("name", "romney");
			INDEXPOSDIR = INDEXPOSDIR.replaceAll("name", "romney");
			INDEXNEGDIR = INDEXNEGDIR.replaceAll("name", "romney");
			break;
		}
	}
	
	public INDEXTYPE getCurrentIndexType() {
		return m_eIndexType;
	}
	
	/**
	 * checks whether index directory exists
	 * @return
	 */
	public boolean exists(){
		
		if(!(new File(INDEXPOSDIR).exists()) ||
				!(new File(INDEXNEGDIR).exists()) ||
						!(new File(INDEXNEUDIR).exists()) ||
								!(new File(INDEXMIXDIR).exists()))
			return false;
		
		return true;
	}

	/**
	 * Create lucene indexes
	 * @param filepath
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public void writeCorpusStoredIndexdir() throws IOException
	{
		Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
		
		
//		Analyzer analyzer = new Analyzer() {
//			
//			@Override
//			protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
//				 Tokenizer source = new NGramTokenizer(reader, 1, 2);
//			     TokenStream filter = new StandardFilter(Version.LUCENE_40, source);
//			     filter = new NGramTokenFilter(filter);
//			     return new TokenStreamComponents(source, filter);
//			}
//		};

		// To store an index on disk
		Directory directory = null;
		String INDEXDIR = "";
		try {

			@SuppressWarnings("unchecked")
			Set<String> classkeys = (Set<String>) m_TweetTupleInstance.getMapTrainTweets().keySet();

			//get the POS for all the tokens in a line and store as it is
			StanfordWrapParser parser = StanfordWrapParser.getInstance();
			for (String ck : classkeys) {

				INDEXDIR = getClassIndexDir(ck);
				List<String> totalSentList = (List<String>) m_TweetTupleInstance.getMapTrainTweets().getCollection(ck);

				File indexdir = new File(INDEXDIR);
				directory = SimpleFSDirectory.open(indexdir);
				IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
				IndexWriter iwriter = new IndexWriter(directory, config);

				Iterator<String> itr = totalSentList.iterator();

				int nLineCount = 1;
				while( itr.hasNext() )
				{
					
					//get the sentences
					String s = itr.next();

					//tokenize and get POS
					List<String> ls = m_Utils.tweetTokenizer(s); //custom made tokenizer
					List<String> POStagarray = new ArrayList<String>(); //parser.tagSentence(StringUtils.join(ls, " "));		

					//convert to lower case
					List<String> lls = new ArrayList<String>();
					for (String token : ls) {
						token = token.toLowerCase();
						lls.add(token);
					}
					
					//add documents
					List<String> fields = Arrays.asList("Tweet", "Class", "POSTags");
					List<String> values = Arrays.asList(StringUtils.join(lls, " "), ck, StringUtils.join(POStagarray, " "));
					//System.out.println(values);
					addDoc(iwriter, fields, values);

					//test whether all data is read
					nLineCount++;
				}

				System.out.print("LineCount: ");
				System.out.println(nLineCount);
				System.out.println("--------------------------");

				iwriter.close();
			}

		} catch (IOException e) {
			e.printStackTrace();

			//remove the corrupt index directory
			if(directory != null)
				directory.deleteFile(INDEXDIR);
		}
	}

	public String getClassIndexDir(String classKey) {
		String INDEXDIR = "";
		
		if(classKey.compareTo("2") == 0)
			INDEXDIR = INDEXMIXDIR;
		else if(classKey.compareTo("1") == 0)
			INDEXDIR = INDEXPOSDIR;
		else if(classKey.compareTo("-1") == 0)
			INDEXDIR = INDEXNEGDIR;
		else if(classKey.compareTo("0") == 0)
			INDEXDIR = INDEXNEUDIR;
		
		return INDEXDIR;
	}
	
	/**
	 * Does search in the stored index of the corpus
	 * @return The search results. null if there was an error.
	 */
	public List<Float> searchCorpus(String searchField, String searchTerm, String classKey) {
		try {		

			String indexdir = getClassIndexDir(classKey);
			Directory directory = SimpleFSDirectory.open(new File(indexdir));
			IndexReader indexreader = DirectoryReader.open(directory);

			//performance is increased if we use only one IndexSearcher for all the searches
			//if( null == m_IndexSearcher )
			IndexSearcher indSearcher = new IndexSearcher(indexreader);
			Analyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);

			//creates the query parser object with the clause in the form of BNF
			String clause = searchField + ":\"" + searchTerm + "\"";
			QueryParser parser = new QueryParser(Version.LUCENE_40, clause, analyzer);

			//creates a query object with the criteria set by the user, if the criteria is not mentioned the previous default field is used
			Query query = parser.parse(clause); 

			// search get the topdocs
			TopScoreDocCollector collector = TopScoreDocCollector.create(100000, true);
			indSearcher.search(query, collector);
			TopDocs topdocs = collector.topDocs();
			
			//evaluate all the counts: tf, df, idf
			List<Float> scores = new ArrayList<Float>();
			int df = topdocs.scoreDocs.length;
			int tfcount = 0;
			//List<Integer> docs = new ArrayList<Integer>();
			for (ScoreDoc sd : topdocs.scoreDocs) {
				tfcount += getCount(searchTerm, indSearcher.doc(sd.doc).get(searchField));
				//docs.add(sd.doc);
			}
			//Collections.sort(docs);
			//System.out.println(docs);
			DefaultSimilarity tfidfsim = new DefaultSimilarity();
			float tf = tfidfsim.tf(tfcount);
			float idf = tfidfsim.idf(df, indexreader.numDocs());
			//add the scores in the order of tf, df and idf
			scores.add(tf);
			scores.add((float)df);
			scores.add(idf);
			
			//close the index
			indexreader.close();
			directory.close();
			
			return scores;
		} 
		catch (ParseException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns the document frequency for the given text and class
	 * @param searchField
	 * @param searchText
	 * @param classKey
	 * @return
	 */
	public int getdocFreq(String searchField, String searchText, String classKey) {
		int df = 0;

//		try {		
//			String INDEXDIR = getClassIndexDir(classKey); 
//			Directory directory = SimpleFSDirectory.open(new File(INDEXDIR));
//			IndexReader indexreader = DirectoryReader.open(directory);
//			df = indexreader.docFreq(new Term(searchField, searchText));
//			
//		} 
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
		return df;
	}
	
	/**
	 * Returns the term frequency for the given text and class
	 * @param searchField
	 * @param searchText
	 * @param classKey
	 * @return
	 */
	public float gettermFreq(String searchField, String searchText, String classKey) {
		
		float tf = 0.0f;
//		try {		
//			String INDEXDIR = getClassIndexDir(classKey); 
//			Directory directory = SimpleFSDirectory.open(new File(INDEXDIR));
//			DirectoryReader indexreader = DirectoryReader.open(directory);
//			
//			float ttf = indexreader.totalTermFreq(new Term(searchField, searchText));
//			DefaultSimilarity tfidfsim = new DefaultSimilarity();
//			tf = tfidfsim.tf(ttf);
//		} 
//		catch (IOException e) {
//			e.printStackTrace();
//		}
		
		//check for NAN
		if(Float.isNaN(tf))
			tf = 0.0f;
		
		return tf;
	}
	
	private int getCount(String regex, String input){
//		String token = "My name is nikhil is nikhil and this is nikhil";
//		String regexpat = "is nikhil";
		
		regex = regex + " "; // since the tokens are space delimited, this makes sure only whole tokens are identified
		Pattern pattern = Pattern.compile("(" + regex + ")");
		Matcher matcher = pattern.matcher(input.toLowerCase());
		int count = 0;
		while(matcher.find()){
			count++;
		}
		
		return count;	
	}
	
	private void addDoc(IndexWriter writer, List<String> fields, List<String> values) throws IOException {
	    Document doc = new Document();
	    
	    //equal number of fields and values
	    for (int i = 0; i < fields.size(); i++ ) {
	    	FieldType fieldType = new FieldType();
		    fieldType.setStoreTermVectors(true);
		    fieldType.setStoreTermVectorPositions(true);
		    fieldType.setIndexed(true);
		    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
		    fieldType.setStored(true);
		    fieldType.setTokenized(true);
		    
	    	doc.add(new Field(fields.get(i), values.get(i), fieldType));	
		}
	    writer.addDocument(doc);
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
//		String token = "My name is nikhil is nikhil and this is sandy";
//		String regexpat = "and";
//		Pattern pattern = Pattern.compile("(" + regexpat + ")");
//		Matcher matcher = pattern.matcher(token);
//		int count = 0;
//		while(matcher.find()){
//			count++;
//		}
//		
//		System.out.println(count);
		
		
		IndexerSearcher sear = IndexerSearcher.getInstance();
		sear.setIndexType(INDEXTYPE.ROMNEY);
		String searchText = "mitt";
		
		List<Float> scores = sear.searchCorpus("Tweet", searchText, "2");
		System.out.println(scores);
//		
//		System.out.println("\nTF\n");
//		System.out.println(sear.gettermFreq("Tweet", searchText, "2"));
//		System.out.println(sear.gettermFreq("Tweet", searchText, "1"));
//		System.out.println(sear.gettermFreq("Tweet", searchText, "0"));
//		System.out.println(sear.gettermFreq("Tweet", searchText, "-1"));
		//System.out.println("\nDF\n");
		//System.out.println(sear.getdocFreq("Tweet", searchText, "2"));
//		System.out.println(sear.getdocFreq("Tweet", searchText, "1"));
//		System.out.println(sear.getdocFreq("Tweet", searchText, "0"));
//		System.out.println(sear.getdocFreq("Tweet", searchText, "-1"));
//		System.out.println("\nIDF\n");
//		System.out.println(sear.getinversedocFreq("Tweet", searchText, "2"));
//		System.out.println(sear.getinversedocFreq("Tweet", searchText, "1"));
//		System.out.println(sear.getinversedocFreq("Tweet", searchText, "0"));
//		System.out.println(sear.getinversedocFreq("Tweet", searchText, "-1"));
//		System.out.println("\nTFIDF\n");
//		System.out.println(sear.gettfidf("Tweet", searchText, "2"));
//		System.out.println(sear.gettfidf("Tweet", searchText, "1"));
//		System.out.println(sear.gettfidf("Tweet", searchText, "0"));
//		System.out.println(sear.gettfidf("Tweet", searchText, "-1"));
	}

}

package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jswordnet.PorterStemmer;
import jswordnet.WordNetUtils;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.lucene.search.IndexSearcher;

import data.IndexerSearcher.INDEXTYPE;

import parser.StanfordWrapParser;

import edu.stanford.nlp.util.StringUtils;

import utils.Utilities;
import utils.Utilities.NGRAM;
import utils.Utilities.SENTIMENT;
import utils.Utilities.TOKENTYPE;

public class TweetTuple {

	private MultiValueMap m_MapTrainTweetData;
	private MultiValueMap m_MapTestTweetData;
	private Map<Integer, List<String>> m_MapTIDAttrList;
	PorterStemmer m_PStemmer;
	WordNetUtils m_WNetutils;
	Utilities m_Utils;
	BuildTFIDFScores m_tfidfInstance;
	StanfordWrapParser parser;
	MultiValueMap m_ClassRepTokensMap;

	private static TweetTuple m_SingleTTInstance = null;

	public enum MODELFILEFORMAT{
		STANFORD,
		SVMLIGHT
	};

	/**
	 * Singleton
	 * @return
	 */
	public static TweetTuple getInstance(){
		if(m_SingleTTInstance == null)
			m_SingleTTInstance = new TweetTuple();

		return m_SingleTTInstance;
	}

	private TweetTuple() {
		m_MapTrainTweetData = new MultiValueMap();
		m_MapTestTweetData = new MultiValueMap();
		m_ClassRepTokensMap = new MultiValueMap();
		m_MapTIDAttrList = new HashMap<Integer, List<String>>();
		m_PStemmer = new PorterStemmer();
		m_WNetutils = WordNetUtils.getInstance();
		m_Utils = new Utilities();
		m_tfidfInstance = null;
		parser = StanfordWrapParser.getInstance();
	}

	public Map<Integer, List<String>> getMapTIDAttrList() {
		return m_MapTIDAttrList;
	}

	public void setMapTIDAttrList(Map<Integer, List<String>> mapTIDAttrList) {
		this.m_MapTIDAttrList = mapTIDAttrList;
	}

	public MultiValueMap getMapTrainTweets() {
		return m_MapTrainTweetData;
	}

	public void setMapTrainTweets(MultiValueMap mapTraindata) {
		this.m_MapTrainTweetData = mapTraindata;
	}

	public MultiValueMap getMapTestTweets() {
		return m_MapTestTweetData;
	}

	public void setMapTestTweets(MultiValueMap mapTestdata) {
		this.m_MapTestTweetData = mapTestdata;
	}

	public Set<String[]> generateFeaturesforTraining(MODELFILEFORMAT eModelFileFormat){
		Set<String[]> train = new LinkedHashSet<String[]>() ;

		switch(eModelFileFormat){
		case STANFORD:
			train = generateSFFeatures();
			break;
		case SVMLIGHT:
			train = generateSVMFeatures();
			break;
		}

		return train;
	}

	public Set<String[]> generateTestData(MODELFILEFORMAT eModelFileFormat, boolean bWithClass){

		Set<String[]> test = new LinkedHashSet<String[]>() ;

		switch(eModelFileFormat){
		case STANFORD:
			test = createSFTestdata(bWithClass);
			break;
		case SVMLIGHT:
			test = createSVMTestData();
			break;
		}

		return test;
	}


	/*
	 * Bag of features f = {f1, f2, f3... fm}
	 */
	@SuppressWarnings("unchecked")
	private Set<String[]> generateSFFeatures(){

		//		find positive/negative/neutral word and topic word
		//		Feature sets:
		//		a. unigram
		//		b. bigram
		//		c. a & b
		//		d. POS tags: NLProcessor linguistic parser [25](Bing liu), TweetNLP(CMU)
		//		e. Semantics of the tweet
		//		f. emoticons
		//		g. Domain of the unigrams and bigrams
		//		h. hedges
		//		i. sentiment scores
		//		j. word relationship score(PMI, loglikelihood, tf, tf-idf)
		//	find the hedges which might reduce the intensity of the impression made in step 1
		//	Identification of better supervised algorithm. Most of the papers have used NB, MaxEnt and SVM. Need to compare results

		//Features
		//a, b
		Set<String[]> traindata = new LinkedHashSet<String[]>();
		Set<String> keys =  m_MapTrainTweetData.keySet();			

		System.out.println("Traindata size: " + m_MapTrainTweetData.values().size());
		System.out.println("Train class 2: " + m_MapTrainTweetData.getCollection("2").size());
		System.out.println("Train class 1: " + m_MapTrainTweetData.getCollection("1").size());
		System.out.println("Train class 0: " + m_MapTrainTweetData.getCollection("0").size());
		System.out.println("Train class -1: " + m_MapTrainTweetData.getCollection("-1").size());

		//get token index and scores
		generateTFIDFFeautres();
		Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTFIDF();
		for (String classkey : keys) {


			Collection<String> classData = m_MapTrainTweetData.getCollection(classkey);
			System.out.println("Train class " + classkey + ": " + classData.size());

			//feature vector generation
			for (String ptweet : classData) {
				//remove usernames
				//remove links
				//remove named entities
				//ptweet = m_Utils.cleanData(ptweet);

				//Generate unigram and bigram features
				//tokenize sentence
				List<String> srcTokens = m_Utils.tweetTokenizer(ptweet);

				//drop emoticons
				//List<Integer> eind = m_Utils.findTokenIndex(srcTokens, TOKENTYPE.EMOTICON);
//				for (Integer i : eind) 
//					srcTokens.remove(i);

				//POS tag
//				List<String> POStagarray = parser.tagSentence(StringUtils.join(srcTokens, " "));
//
//				//add user-defined token
//				for (Integer i : eind)
//					POStagarray.add(i, "EMO");

				List<String> uFeatures = m_Utils.constructNGrams(srcTokens, NGRAM.UNIGRAM);
				List<String> ucleanUFeatures = discardUselessFeatures(uFeatures, NGRAM.UNIGRAM);

				List<String> bFeatures = m_Utils.constructNGrams(srcTokens, NGRAM.BIGRAM);
				List<String> bcleanUFeatures = discardUselessFeatures(bFeatures, NGRAM.BIGRAM);

				//no data, continue
				if(ucleanUFeatures.isEmpty() || bcleanUFeatures.isEmpty())
					continue;

				List<String> joinedFeatures = new ArrayList<String>();
				joinedFeatures.add(classkey);

				String u = StringUtils.join(ucleanUFeatures, "|||");
				if(StringUtils.isPunct(u))
					continue;

				joinedFeatures.add(u);
				joinedFeatures.add(StringUtils.join(bcleanUFeatures, "|||"));
				//joinedFeatures.add(StringUtils.join(POStagarray, "|||"));

				//tfidf score feature
				List<Double> tfidffv = new ArrayList<Double>();
				double tfidf = 0.0;

				for (String token : srcTokens) {
					Map<String, Float> scoreMap = irScoreMap.get(token.toLowerCase());
					if(null == scoreMap)
						continue;
					float val = Float.parseFloat(String.format("%.2f", scoreMap.get(classkey)));
					tfidf += val;
				}

				//avg tfidf for the tweet
				tfidf = tfidf / srcTokens.size();

				//add into train data
				joinedFeatures.add(String.valueOf(tfidf));
				traindata.add(joinedFeatures.toArray(new String[joinedFeatures.size()]));
			}
		}

		return traindata;
	}

	private Set<String[]> createSFTestdata(boolean bwithClass){

		Set<String[]> testdata = new LinkedHashSet<String[]>();

		System.out.println("Testdata size: " + m_MapTestTweetData.values().size());
		Set<String> keys =  m_MapTestTweetData.keySet();
		Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTFIDF();
		for (String classkey : keys) {

			//skip the headers
			if(classkey.toLowerCase().contains("class"))
				continue;

			Collection<String> classData = m_MapTestTweetData.getCollection(classkey);	
			//generate feature vector
			for (String ptweet : classData) {
				//remove usernames
				//remove links
				//remove named entities
				//ptweet = m_Utils.cleanData(ptweet);

				//Generate unigram features
				//tokenize sentence
				List<String> srcTokens = m_Utils.tweetTokenizer(ptweet);

				//remove emoticon
//				List<Integer> eind = m_Utils.findTokenIndex(srcTokens, TOKENTYPE.EMOTICON);
//				for (Integer i : eind)
//					srcTokens.remove(i);
//
//				//POS tag
//				List<String> POStagarray = parser.tagSentence(ptweet);

				//add user-defined tokentype
//				for (Integer i : eind)
//					POStagarray.add(i, "EMO");

				List<String> uFeatures = m_Utils.constructNGrams(srcTokens, NGRAM.UNIGRAM);
				List<String> ucleanUFeatures = discardUselessFeatures(uFeatures, NGRAM.UNIGRAM);

				List<String> bFeatures = m_Utils.constructNGrams(srcTokens, NGRAM.BIGRAM);
				List<String> bcleanUFeatures = discardUselessFeatures(bFeatures, NGRAM.BIGRAM);

				//no data, continue
				if(ucleanUFeatures.isEmpty() || bcleanUFeatures.isEmpty())
					continue;

				List<String> joinedFeatures = new ArrayList<String>(); 
				if(bwithClass)
					joinedFeatures.add(classkey);

				String b = StringUtils.join(ucleanUFeatures, "|||");
				if(StringUtils.isPunct(b))
					continue;

				joinedFeatures.add(b);
				joinedFeatures.add(StringUtils.join(bcleanUFeatures, "|||"));
				//joinedFeatures.add(StringUtils.join(POStagarray, "|||"));

				//tfidf score feature
				List<Double> tfidffv = new ArrayList<Double>();
				double tfidf = 0.0;
				for (String token : srcTokens) {
					Map<String, Float> scoreMap = irScoreMap.get(token.toLowerCase());

					if(scoreMap == null || !scoreMap.containsKey(classkey))
						tfidf += (float)1.0 / keys.size(); //giving equal opportunity for all the 4 classes 
					else{
						float val = Float.parseFloat(String.format("%.2f", scoreMap.get(classkey)));
						tfidf += val;
					}
				}

				tfidf = tfidf / srcTokens.size();

				//add into test data
				joinedFeatures.add(String.valueOf(tfidf));
				testdata.add(joinedFeatures.toArray(new String[joinedFeatures.size()]));
			}
		}

		return testdata;
	}

	private Set<String[]> generateSVMFeatures(){

		Set<String[]> traindata = new LinkedHashSet<String[]>();
		Set<String> keys =  m_MapTrainTweetData.keySet();			
		generateTFIDFFeautres();
		//Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTFIDF();
		Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTF();
		Map<String, Integer> tokenIndexMap = m_tfidfInstance.getM_allTokensMapIndex();

		String filename = "traininput.txt";
		writeIntoFile(filename, "traindata\n", false);
		for (String classkey : keys) {

			//skip the headers
			if(classkey.toLowerCase().contains("class"))
				continue;

			Collection<String> classData = m_MapTrainTweetData.getCollection(classkey);	

			
			
			//generate feature vector
			for (String ptweet : classData) {

				List<String> joinedFeatures = new ArrayList<String>();

				//remove usernames
				//remove links
				//remove named entities
				//ptweet = m_Utils.cleanData(ptweet);

				//tokenize sentence
				List<String> tokens = m_Utils.tweetTokenizer(ptweet.trim());
				writeIntoFile(filename, ptweet+"\n", true);

				//ngram features
				List<String> srcTokens = new ArrayList<String>();
				srcTokens.addAll(tokens);
				//srcTokens.addAll(m_Utils.constructNGrams(tokens, NGRAM.BIGRAM));

				//tfidf score feature
				List<String> tfidffv = new ArrayList<String>();
				Map<Integer, String> tempMap = new HashMap<Integer, String>();
				List<Integer> tokenindexList = new ArrayList<Integer>(); 
				for(String token : srcTokens){
					if(!tokenIndexMap.containsKey(token.toLowerCase()))
						continue;
					int index = tokenIndexMap.get(token.toLowerCase());
					if(!tempMap.containsKey(index)){
						tempMap.put(index, token);
						tokenindexList.add(index);
					}
				}

				//sort the index
				Collections.sort(tokenindexList);

				float tfidf = 0.0f;
				for (int i : tokenindexList) {
					String token = tempMap.get(i);
					Map<String, Float> scoreMap = irScoreMap.get(token.toLowerCase());

					//only consider features greater than 0.2
					if(scoreMap.get(classkey) <= 0.20f)
						continue;
					
					//save the representative features(tokens)
					if(scoreMap.get(classkey) >= 0.4f)
						m_ClassRepTokensMap.put(classkey, token);
					
					tfidf = Float.parseFloat(String.format("%.4f", scoreMap.get(classkey)));
					//check for NaN
					if(Float.isNaN(tfidf))
						tfidf = 0.0f;

					int tokenIndex = tokenIndexMap.get(token.toLowerCase());
					String stfidf = String.valueOf(tokenIndex) + ":" + String.valueOf(tfidf);
					tfidffv.add(stfidf);
				}

				//since svm wont take negative or zero as class labels
				//converting 0 => 3 and -1 => 4
				String c = classkey;
				if(classkey.toLowerCase().trim().equals("0"))
					c = "3";
				else if(classkey.toLowerCase().trim().equals("-1"))
					c = "4";
				joinedFeatures.add(c);
				
				joinedFeatures.add(StringUtils.join(tfidffv, " "));

				//add into train data
				traindata.add(joinedFeatures.toArray(new String[joinedFeatures.size()]));
			}
		}

		return traindata;
	}

	private Set<String[]> createSVMTestData(){

		Set<String[]> testdata = new LinkedHashSet<String[]>();
		Set<String> keys =  m_MapTestTweetData.keySet();			
		Map<String, Integer> tokenIndexMap = m_tfidfInstance.getM_allTokensMapIndex();
		//Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTFIDF();
		Map<String, Map<String, Float>> irScoreMap = m_tfidfInstance.getM_featTF();

		String filename = "testinput.txt";
		writeIntoFile(filename, "testdata\n", false);
		for (String classkey : keys) {

			//skip the headers
			if(classkey.toLowerCase().contains("class"))
				continue;

			Collection<String> classData = m_MapTestTweetData.getCollection(classkey);				
			//generate feature vector
			for (String ptweet : classData) {

				List<String> joinedFeatures = new ArrayList<String>();

				//remove usernames
				//remove links
				//remove named entities
				//ptweet = m_Utils.cleanData(ptweet);

				//tokenize sentence
				List<String> tokens = m_Utils.tweetTokenizer(ptweet.trim());
				writeIntoFile(filename, ptweet+"\n", true);

				//ngram features
				List<String> srcTokens = new ArrayList<String>();
				srcTokens.addAll(tokens);
				//srcTokens.addAll(m_Utils.constructNGrams(tokens, NGRAM.BIGRAM));

				//tfidf score feature
				List<String> tfidffv = new ArrayList<String>();
				Map<Integer, String> tempMap = new HashMap<Integer, String>();
				List<Integer> tokenindexList = new ArrayList<Integer>(); 
				for(String token : srcTokens){
					if(!tokenIndexMap.containsKey(token.toLowerCase()))
						continue;
					int index = tokenIndexMap.get(token.toLowerCase());
					if(!tempMap.containsKey(index)){
						tempMap.put(index, token);
						tokenindexList.add(index);
					}
				}

				//sort the index
				Collections.sort(tokenindexList);

				//get the best key from the tokens
				//String bestKey = getBestMatchedClass(srcTokens, irScoreMap);
				
				float tfidf = 0.0f;
				for (int i : tokenindexList) {
					String token = tempMap.get(i);
					Map<String, Float> scoreMap = irScoreMap.get(token.toLowerCase());

					if(scoreMap == null)
						tfidf = (float)1.0 / keys.size(); //giving equal opportunity for all the 4 classes 
					else{
						
						//taking the max
						 if(scoreMap.containsKey("2")){
							 tfidf = scoreMap.get("2");
						 }
						 
						 if(scoreMap.containsKey("1")){
							 if(tfidf < scoreMap.get("1"))
								 tfidf = scoreMap.get("1");
						 }

						 if(scoreMap.containsKey("0")){
							 if(tfidf < scoreMap.get("0"))
								 tfidf = scoreMap.get("0");
						 }

						 if(scoreMap.containsKey("-1")){
							 if(tfidf < scoreMap.get("-1"))
								 tfidf = scoreMap.get("-1");
						 }		
						 
						//get the score
//						if(null != scoreMap)
//							tfidf = scoreMap.get(bestKey);
						 
						 //if not present in any of the classes, giving equal opportunity for all the 4 classes 
						 if(tfidf == 0.0f)
							 tfidf = 0.25f;
						 
//						//only consider features greater than 0.2
						if(tfidf <= 0.20f)
							continue;

						//tfidf = scoreMap.get(classkey);
						tfidf = Float.parseFloat(String.format("%.4f", tfidf));
						
						//check for NaN
						if(Float.isNaN(tfidf))
							tfidf = 0.0f;
					}

					int tokenIndex = tokenIndexMap.get(token.toLowerCase());
					String stfidf = String.valueOf(tokenIndex) + ":" + String.valueOf(tfidf);
					tfidffv.add(stfidf);
				}

				//since svm wont take negative or zero as class labels
				//converting 0 => 3 and -1 => 4
				String c = classkey;
				if(classkey.toLowerCase().trim().equals("0"))
					c = "3";
				else if(classkey.toLowerCase().trim().equals("-1"))
					c = "4";
				joinedFeatures.add(c);
				joinedFeatures.add(StringUtils.join(tfidffv, " "));

				//add into train data
				testdata.add(joinedFeatures.toArray(new String[joinedFeatures.size()]));
			}
		}

		return testdata;
	}

	private String getBestMatchedClass(List<String> tokens, Map<String, Map<String, Float>> irScoreMap){

		//do operation on copied tokenlist
		List<String> copyTokens = new ArrayList<String>(tokens);
		String C = new String();
		Map<String, Float> potentialClasses = new HashMap<String, Float>();
		Map<String, Float> cumScoreMap = new HashMap<String, Float>();
		Set<String> keys = m_MapTestTweetData.keySet();
		boolean bFind = false;
		List<String> potentialTokens = new ArrayList<String>();
		while(!bFind){
			potentialTokens.clear();
			float val = 0.0f;
			for (String token : copyTokens) {
				for (String ck : keys) {
					Map<String, Float> scoreMap = irScoreMap.get(token.toLowerCase());
					if(scoreMap != null)
						val = scoreMap.get(ck);

					//cumulative score
					if(val > 0.4f){
						if(!cumScoreMap.containsKey(ck))
							cumScoreMap.put(ck, val);
						else
							cumScoreMap.put(ck, val + cumScoreMap.get(ck));
					}
					
					//add potential classes
					if(m_ClassRepTokensMap.containsValue(ck, token)){
						if((potentialClasses.containsKey(ck) && potentialClasses.get(ck) < val) ||
								!potentialClasses.containsKey(ck)){
							potentialClasses.put(ck, val);
							potentialTokens.add(token);
						}
					}
				}
			}
			
			//check if all values are same
			//out of them identify the highest tokens' score class
			float high = 0.0f;
			C = "";
			for (String ck : keys) {
				if(potentialClasses.containsKey(ck)){
					if( high < potentialClasses.get(ck)){
						high = potentialClasses.get(ck);
						C = ck;
					}
					else if( high == potentialClasses.get(ck)){
						copyTokens.removeAll(potentialTokens);
						potentialClasses.clear();
						C = "";
						break;
					}
				}
			}
			
			//if potential tokens does not exist break
			if(potentialTokens.isEmpty() || !C.isEmpty())
				bFind = true;
		}
		
		//check if C is empty
		potentialClasses.clear();
		float max = 0.0f;
		if(C.isEmpty()){
			for(String ck : cumScoreMap.keySet()) {
				if(max < cumScoreMap.get(ck))
					max = cumScoreMap.get(ck);
					C = ck;
			}
		}
			
		//still C is empty
		if(C.isEmpty()){
			int rand = (int) (Math.random()*100);
			int randkey = rand % 4;

			switch(randkey){
			case 0: 
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				randkey = -1;
				break;
			}

			C = String.valueOf(randkey);
		}
		
		return C;
	}
	
	/**
	 * Remove features which are useless to the sentiment 'senti'
	 * @param features
	 * @param senti
	 * @param ngram 
	 * @return
	 */
	private List<String> discardUselessFeatures(List<String> features, NGRAM ngram){

		List<String> cleanFeatures = new ArrayList<String>();
		try {
			switch (ngram){
			case UNIGRAM:
				for (String ugram : features) {

					//remove stopwords
					ugram = m_WNetutils.removeNoise(ugram);

					//					//stem it
					//					m_PStemmer.stem(ugram);
					if(ugram.isEmpty())
						continue;

					cleanFeatures.add(ugram);
				}
				break;
			case BIGRAM:
				for (String bgram : features) {

					//remove stopwords
					bgram = m_WNetutils.removeNoise(bgram);
					//					//stem it
					//					m_PStemmer.stem(bgram);

					//if empty or not bigram
					if(bgram.isEmpty() || bgram.split(" ").length < 2)
						continue;

					cleanFeatures.add(bgram);
				}
				break;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cleanFeatures;
	}

	private void generateTFIDFFeautres(){
		String serializeddata = new String();
		IndexerSearcher indsear = IndexerSearcher.getInstance();
		INDEXTYPE type = indsear.getCurrentIndexType();
		switch(type){
		case OBAMA:
			serializeddata = "tfidf_uni_bi_obama.ser";
			break;
		case ROMNEY:
			serializeddata = "tfidf_uni_bi_romney.ser";
			break;
		}
		
		//accordingly load the data
		if(new File(serializeddata).exists()){
			loadTFIDFFeatures(serializeddata);
		}
		else{
			m_tfidfInstance = new BuildTFIDFScores();
			m_tfidfInstance.init();
			FileOutputStream fout;
			try {
				fout = new FileOutputStream(serializeddata);
				ObjectOutputStream oos = new ObjectOutputStream(fout);
				oos.writeObject(m_tfidfInstance);
				oos.flush();
				oos.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//into file
		//writeTFIDFScoresIntoFile();
	}

	private void loadTFIDFFeatures(String serializeddata){
		FileInputStream fin = null;
				
		//accordingly load the data
		if( new File(serializeddata).exists()){
			try {
				fin = new FileInputStream(serializeddata);
				ObjectInputStream ois = new ObjectInputStream(fin);
				m_tfidfInstance = (BuildTFIDFScores)ois.readObject();
				ois.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void writeIntoFile(String filename, String data, boolean bappend){

		try {
			FileOutputStream out = new FileOutputStream(new File(filename), bappend);
			out.write(data.getBytes());
			out.close();			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void writeTFIDFScoresIntoFile(){
		String data = new String();

		//write indexes into separate file
		Map<String, Integer> tokenindexMap = m_tfidfInstance.getM_allTokensMapIndex();
		List<String> alltokensinMap = new ArrayList<String>( tokenindexMap.keySet() );
		data = "Index, Token\n";
		for (String tok : alltokensinMap) {
			data += tokenindexMap.get(tok) + "," + tok  +  "\n";
		}

		//write index into file
		writeIntoFile("index.txt", data, false);

		//write tf, idf tfidf, class into file
		String filename = "tf_uni_bi.txt";
		List<String> classkeys = new ArrayList<String>( m_MapTrainTweetData.keySet());
		data = "Token, TF, Class\n"; // "Token, TF, IDF, TFIDF, Class\n";
		for (String key : classkeys) {
			List<String> tweetList = (List<String>) m_MapTrainTweetData.getCollection(key);
			for(String ptweet: tweetList){
				List<String> tokens = m_Utils.tweetTokenizer(ptweet);
				for (String token : tokens) {
					if(m_tfidfInstance !=  null){
						token = token.toLowerCase();
						//float idfvalue =  m_tfidfInstance.getIDFScore(token, key);
						float tfvalue =  m_tfidfInstance.getTFScore(token, key);
						//float tfidfvalue =  m_tfidfInstance.getTFIDFScore(token, key);
						data += token + "," + tfvalue /*+ "," + idfvalue + "," + tfidfvalue*/ + "," + key + "\n";
					}
				}
			}
		}

		//writing into file
		writeIntoFile(filename, data, false);
	}
}

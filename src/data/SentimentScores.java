package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import edu.smu.tspell.wordnet.SynsetType;

import jswordnet.WordNetUtils;

import utils.Utilities;

public class SentimentScores {

	private String m_PosFileName;
	private String m_NegFileName;
	private List<String> m_PosWordsList;
	private List<String> m_NegWordsList;
	private static SentimentScores m_SentimentIns = null;

	public static SentimentScores getInstance(){	
		if(m_SentimentIns == null)
			m_SentimentIns = new SentimentScores();

		return m_SentimentIns;
	}

	private SentimentScores() {
		m_PosFileName = "./sentimentwords/positive-words.txt";
		m_NegFileName = "./sentimentwords/negative-words.txt";
		loadSentimentWords();
	}

	private void loadSentimentWords(){
		File posFile = new File(m_PosFileName);
		File negFile = new File(m_NegFileName);
		try {
			//positive words
			BufferedReader reader = new BufferedReader(new FileReader(posFile));
			String data = new String();
			while( (data = reader.readLine()) != null){
				if(data.startsWith(";"))
					continue;
				m_PosWordsList.add(data);
			}

			//negative words
			reader = new BufferedReader(new FileReader(negFile));
			data = new String();
			while( (data = reader.readLine()) != null){
				if(data.startsWith(";"))
					continue;
				m_NegWordsList.add(data);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	float calculateSentimentOrientation(String[] srcTokens){
		float score = 0.0f;

		WordNetUtils wnutils = WordNetUtils.getInstance();
		for (String token : srcTokens) {
			if(m_PosWordsList.contains(token))
				score += 1.0;
			if(m_NegWordsList.contains(token))
				score += -1.0;
			
			//identify sentiment
			for (String pos : m_PosWordsList) {
				if(wnutils.bAreSynonyms(pos, token, SynsetType.NOUN)){
					score += 1.0;
					break;
				}
			}
			
			for (String pos : m_NegWordsList) {
				if(wnutils.bAreSynonyms(pos, token, SynsetType.NOUN)){
					score += -1.0;
					break;
				}
			}
		}
		
		
		
		return score;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {


	}

}

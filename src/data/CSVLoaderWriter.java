package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;

import data.IndexerSearcher.INDEXTYPE;
import data.TweetTuple.MODELFILEFORMAT;

import utils.Utilities;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CSVLoaderWriter {

	private String m_IPFileName;
	private TweetTuple m_TweetTupleInstance;
	private Utilities m_Utils;
	IndexerSearcher m_IndexerSearcherIns;
	
	public CSVLoaderWriter() {
		m_TweetTupleInstance = TweetTuple.getInstance();
		m_Utils = new Utilities();
		m_IndexerSearcherIns = IndexerSearcher.getInstance();
	}
	
	public TweetTuple getTweetTupleInstance() {
		return m_TweetTupleInstance;
	}
	
	void loadAndParitionTweets(String filename){		
		try {
		
			//read the whole data
			CSVReader reader = new CSVReader(new FileReader(filename), '\t');
			String[] nextLine;
			List<String[]> wholedata = new ArrayList<String[]>();
			while ((nextLine = reader.readNext()) != null) {		    	
				wholedata.add(nextLine);
			}
			
			System.out.println(wholedata.size());
			
			//split 70%-training, 30%-testing
			// if you change the percent from previous run to current run, then delete the tfidf.ser to recreate the model
			float trainpercent = 0.7f;
			List<List<String[]>> trainAndTestSet = m_Utils.createRandomParition(trainpercent, wholedata);
			
			//store into trainset, 
			List<String[]> trainDataList = trainAndTestSet.get(0);
			MultiValueMap trainTweetMap = new MultiValueMap();
			for (String[] traintweet : trainDataList) {
				if(traintweet[1].trim().toLowerCase().compareTo("class") == 0)
					continue;
				trainTweetMap.put(traintweet[1].trim(), traintweet[0]);
			}
			
			//store into test set
			List<String[]> testDataList = trainAndTestSet.get(1);
			MultiValueMap testTweetMap = new MultiValueMap();
			for (String[] testtweet : testDataList) {	
				if(testtweet[1].trim().toLowerCase().compareTo("class") == 0)
					continue;
				testTweetMap.put(testtweet[1].trim(), testtweet[0]);
			}
			

			//save the tweet, class
			m_TweetTupleInstance.setMapTrainTweets(trainTweetMap);
			m_TweetTupleInstance.setMapTestTweets(testTweetMap);
			
			//create index only if not created earlier
			INDEXTYPE eIndType = null;
			if(filename.contains("Obama"))
				eIndType = INDEXTYPE.OBAMA;
			else if(filename.contains("Romney"))
				eIndType = INDEXTYPE.ROMNEY;
			
			//set the index type
			m_IndexerSearcherIns.setIndexType(eIndType);
			if(!m_IndexerSearcherIns.exists())	
				m_IndexerSearcherIns.writeCorpusStoredIndexdir();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void loadTrainTweets(String filename){		
		try {
		
			//read the whole data
			CSVReader reader = new CSVReader(new FileReader(filename), '\t');
			String[] nextLine;
			List<String[]> wholedata = new ArrayList<String[]>();
			while ((nextLine = reader.readNext()) != null) {		    	
				wholedata.add(nextLine);
			}
			
			System.out.println("Train data size: " + String.valueOf(wholedata.size()-1));
			
			//For split: 70%-training, 30%-testing
			// if you change the percent from previous run to current run, then delete the tfidf.ser to recreate the model
			//float trainpercent = 0.7f;
			//List<List<String[]>> trainAndTestSet = m_Utils.createRandomParition(trainpercent, wholedata);
			//List<String[]> trainDataList = trainAndTestSet.get(0);
			
			List<String[]> trainDataList = wholedata; //use this for total training
			MultiValueMap trainTweetMap = new MultiValueMap();
			for (String[] traintweet : trainDataList) {
				if(traintweet[1].trim().toLowerCase().compareTo("class") == 0)
					continue;
				trainTweetMap.put(traintweet[1].trim(), traintweet[0]);
			}
			
			//save the tweet, class
			m_TweetTupleInstance.setMapTrainTweets(trainTweetMap);
						
			//create index only if not created earlier
			INDEXTYPE eIndType = null;
			if(filename.contains("Obama"))
				eIndType = INDEXTYPE.OBAMA;
			else if(filename.contains("Romney"))
				eIndType = INDEXTYPE.ROMNEY;
			
			//set the index type
			m_IndexerSearcherIns.setIndexType(eIndType);
			if(!m_IndexerSearcherIns.exists())	
				m_IndexerSearcherIns.writeCorpusStoredIndexdir();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	void loadTestTweets(String filename){		
		try {
		
			//read the whole data
			CSVReader reader = new CSVReader(new FileReader(filename), '\t');
			String[] nextLine;
			List<String[]> wholedata = new ArrayList<String[]>();
			while ((nextLine = reader.readNext()) != null) {		    	
				wholedata.add(nextLine);
			}
			
			System.out.println("Test data size: " + String.valueOf(wholedata.size()-1));
			
			//store into testset, 
			List<String[]> testDataList = wholedata;
			MultiValueMap testTweetMap = new MultiValueMap();
			for (String[] testtweet : testDataList) {
				//missing data and header are skipped
				if(testtweet.length < 2 || testtweet[1].trim().toLowerCase().compareTo("class") == 0)
					continue;
				testTweetMap.put(testtweet[1].trim(), testtweet[0]);
			}
			
			//save the tweet, class
			m_TweetTupleInstance.setMapTestTweets(testTweetMap);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeData(Set<String[]> data, String outFilename, MODELFILEFORMAT eModelFileFormat){
			
		//default values
		char sep = '\0';  
		char quotechar = '\0';
		char escapechar = '\0';
		if(MODELFILEFORMAT.STANFORD == eModelFileFormat)
			sep = '\t'; 
		else if(MODELFILEFORMAT.SVMLIGHT == eModelFileFormat)
			sep = ' ';
		
		try {
			// use FileWriter constructor that specifies open for appending
			CSVWriter out = new CSVWriter(new FileWriter(outFilename, false), sep, quotechar, escapechar);
			for (String[] line : data) {
				out.writeNext(line);
			}
				
			//close the handle
			out.close();
						
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CSVLoaderWriter loaderwriter = new CSVLoaderWriter();
		
		//load train and test data
		//Obama or Romney
		String candidate = "Romney";
		loaderwriter.loadTrainTweets("./data/final/train/" + candidate + "_train.tsv");
		loaderwriter.loadTestTweets("./data/final/test/" + candidate + "_test.tsv");
		
		//loaderwriter.loadAndParitionTweets("./data/Obama.tsv");
		//loaderwriter.loadAndParitionTweets("./data/Romney.tsv");
					
		//generate features: svmlight format
		TweetTuple tw = loaderwriter.getTweetTupleInstance();
		Set<String[]> traindata = tw.generateFeaturesforTraining(MODELFILEFORMAT.SVMLIGHT);
		Set<String[]> testWithClass = tw.generateTestData(MODELFILEFORMAT.SVMLIGHT, true);	
		//write features into a file	
		loaderwriter.writeData(traindata, "./trainsvm_" + candidate + ".tsv", MODELFILEFORMAT.SVMLIGHT);
		loaderwriter.writeData(testWithClass, "./testwithclasssvm_" + candidate + ".tsv", MODELFILEFORMAT.SVMLIGHT);
		
		/*
		//generate features: Stanford classifier format
		//Set<String[]>traindata = tw.generateFeaturesforTraining(MODELFILEFORMAT.STANFORD);
		//Set<String[]>testWithClass = tw.generateTestData(MODELFILEFORMAT.STANFORD,true);
		//Set<String[]>testWOClass = tw.generateTestData(MODELFILEFORMAT.STANFORD,false);
		//write features into a file	
		//loaderwriter.writeData(traindata, "./lib/stanfordclassifier/train.tsv", MODELFILEFORMAT.STANFORD);
		//loaderwriter.writeData(testWithClass, "./lib/stanfordclassifier/testwithclass.tsv", MODELFILEFORMAT.STANFORD);
		//loaderwriter.writedata(testWOdata, "./test.tsv"); */
	}
}

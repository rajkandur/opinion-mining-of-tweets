package ml;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import edu.stanford.nlp.classify.SVMLightClassifierFactory;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.AbstractFileLoader;
import weka.core.converters.AbstractFileSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;

public class WekaClassifier {

	private String m_arffPath = null;
	private Instances m_ins = null;
	private Instances m_filteredins = null;
	private Classifier m_dtree = null;

	public WekaClassifier() {
		m_arffPath = new String();
	}

	public String getM_arffPath() {
		return m_arffPath;
	}

	/***
	 * create arff from input
	 * @param path
	 */
	boolean prepareData(String csvpath, String newarffpath){

		File file = new File(csvpath);

		//check the file existence		
		if(!file.exists()) return false;

		//get the loader for csv
		AbstractFileLoader loader = ConverterUtils.getLoaderForFile(file);	
		if(null == loader)
			return false;

		try {
			loader.setSource(file);
			Instances ins = loader.getDataSet();

			//set the class index
			if (ins.classIndex() == -1)
				ins.setClassIndex(0);

			//get the saver for arff
			AbstractFileSaver arffsaver = ConverterUtils.getSaverForExtension(".arff");
			if(null == arffsaver)
				return false;

			//save into arff
			m_arffPath = newarffpath;
			File newfile = new File(newarffpath);
			arffsaver.setFile(newfile);
			arffsaver.setInstances(ins);
			arffsaver.writeBatch();
			
			//save the instances
			m_ins = arffsaver.getInstances();
			//set the class index
			if (m_ins.classIndex() == -1)
				m_ins.setClassIndex(m_ins.numAttributes() - 1);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	void selectBestAttributes(){
		try {
			if(null == m_ins)
				return;
			
			AttributeSelection filter = new AttributeSelection();  // package weka.filters.supervised.attribute!
			CfsSubsetEval eval = new CfsSubsetEval();
			GreedyStepwise search = new GreedyStepwise();
			search.setSearchBackwards(true);
			filter.setEvaluator(eval);
			filter.setSearch(search);
			filter.setInputFormat(m_ins);

			// generate new data
			Instances newData = Filter.useFilter(m_ins, filter);
			
			//new file
			AbstractFileSaver svmlightsaver = ConverterUtils.getSaverForExtension(".dat");
			if(null == svmlightsaver)
				return;
			
			String arffpath = "./mainatt.dat";
			File newfile = new File(arffpath);
			svmlightsaver.setFile(newfile);
			svmlightsaver.setInstances(newData);
			svmlightsaver.writeBatch();
			
			//save the instances
			m_filteredins = svmlightsaver.getInstances();
			//set the class index
			if (m_filteredins.classIndex() == -1)
				m_filteredins.setClassIndex(0);
			
			//into stdout
			//System.out.println(newData);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * train the model
	 */
	boolean buildClassifier(){
		try {
			String[] options = new String[1];
			options[0] = "-U"; //unpruned tree

			//there are no instances
			if(null == m_ins)
				return false;

			//create a decision tree
			m_dtree = new J48();
			m_dtree.setOptions(options);
			m_dtree.buildClassifier(m_ins);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Validating the gold standards
	 * Note: The classifier should not be trained while calling cross-validation. (here am using classifier string)
	 */
	void crossfoldvalidate(boolean bFiltered){
		try {
			
			Instances ins;
			if(bFiltered)
				ins = m_filteredins;
			else
				ins = m_ins;
			
			if(null == ins)
				System.out.println("Load the model before doing cross-validation.");

			Evaluation eval = new Evaluation(ins);
			eval.crossValidateModel(new J48(), ins, 5, new Random(1));
			System.out.println(eval.toSummaryString("\nResults\n======\n", true));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		WekaClassifier c = new WekaClassifier();
		c.prepareData("./testwithclasssvm_Romney.dat", "./trainsvm_Romney.arff");
		c.selectBestAttributes();
		//c.buildClassifier();
		//c.crossfoldvalidate();
	}
}

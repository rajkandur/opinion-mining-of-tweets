package parser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.map.MultiValueMap;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.parser.chunking.Parser;
import opennlp.tools.parser.Parse;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.Span;

public class OpenNLPWrapParser  {

	private InputStream m_tokenStream;
	private TokenizerModel m_tokenmodel;
	private TokenizerME m_tokenizer;

	private InputStream  m_parsein;
	ParserModel m_parsemodel;
	Parser m_qaparser;

	private InputStream  m_posin;
	POSModel m_posmodel;
	POSTaggerME m_postagger;

	static OpenNLPWrapParser m_ONLPParser;

	double gamesScore, movieScore, geographyScore;

	int PARSECOUNT;

	private OpenNLPWrapParser() {

		try {


			PARSECOUNT = 4;

			//create a tokenizer
			m_tokenStream = new FileInputStream(".\\models\\opennlp\\en-token.bin");
			m_tokenmodel = new TokenizerModel(m_tokenStream);
			m_tokenizer =  new TokenizerME(m_tokenmodel);

			//create a parser
			m_parsein = new FileInputStream(".\\models\\opennlp\\en-parser-chunking.bin");
			m_parsemodel = new ParserModel(m_parsein);
			m_qaparser = (Parser) ParserFactory.create(m_parsemodel);

			//create a postagger
			m_posin = new FileInputStream(".\\models\\opennlp\\en-pos-maxent.bin");
			m_posmodel = new POSModel(m_posin);
			m_postagger = new POSTaggerME(m_posmodel);


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static OpenNLPWrapParser getInstance(){
		if( null == m_ONLPParser)
			m_ONLPParser = new OpenNLPWrapParser();	
		return m_ONLPParser;
	}

	public String[] tokenizeSentence(String s){
		String[] tokens = m_tokenizer.tokenize(s);
		return tokens;		
	}

	public String[] parseSentence(String s) {

		//tokenize sentence
		String tokenedSentence =  new String();
		String[] tokens = tokenizeSentence(s);

		for (int j = 0; j < tokens.length-1; j++)
		{
			tokenedSentence += tokens[j];
			tokenedSentence += " ";
		}

		//generate parse trees
		Parse[] parseArray = null;
		parseArray = ParserTool.parseLine(tokenedSentence, m_qaparser, PARSECOUNT);

		//for(Parse parse: parseArray)
		//parse.show();
		double max = 0;
		int maxIndex = 0;
		String[] parseStr = new String[PARSECOUNT];
		for(int i = 0; i < parseArray.length; i++)
		{
			//if( parseArray[i].getProb() > max )
			//{
			//max = parseArray[i].getProb();
			//maxIndex = i;
			StringBuffer buffer = new StringBuffer();
			parseArray[i].show(buffer);
			parseStr[i] = buffer.toString();
			//}
		}

		//		StringBuffer buffer = new StringBuffer();
		//		parseArray[maxIndex].show(buffer);
		//		parseStr[0] = buffer.toString();

		return parseStr; 
	}

	public String[] POStagSentence(String[] sent){
		return m_postagger.tag(sent);
	}

	private List<String> identifyPOS(String[] tagsOfSentence, String[] tokenedSent, List<String> POStags){

		//extract the POS tags and generate the score from the table data
		List<Integer> POSIndexes = new ArrayList<Integer>();

		for(String POS: POStags){
			for(int i = 0; i < tagsOfSentence.length; i++){
				if( tagsOfSentence[i].contains(POS) )
					POSIndexes.add(i);
			}
		}

		//get all the noun forms in the sentence
		List<String> POSWords = new ArrayList<String>();
		for(int index: POSIndexes)
			POSWords.add(tokenedSent[index]);

		return POSWords;
	}

	public List<String> getPOSWordsofSentence(String sent, List<String> POStags){

		//tokenize and get the POS tags
		String[] tokenedSent = tokenizeSentence(sent);
		String[] tagsOfSentence = POStagSentence(tokenedSent);

		List<String> POSWords = identifyPOS(tagsOfSentence, tokenedSent, POStags);
		return POSWords;
	}

	public MultiValueMap findNamedEntities(List<String> sentences) {

		ArrayList<InputStream> models = new ArrayList<InputStream>(); 

		//store all the entities
		MultiValueMap multimap = new MultiValueMap();

		// Read the model file
		InputStream personStream = null;
		InputStream dateStream = null;
		InputStream locationStream = null;
		InputStream orgStream = null;
		InputStream timeStream = null;

		try {
			personStream = new FileInputStream(".\\models\\opennlp\\en-ner-person.bin");
			models.add(personStream);
			dateStream = new FileInputStream(".\\models\\opennlp\\en-ner-date.bin");
			models.add(dateStream);
			locationStream = new FileInputStream(".\\models\\opennlp\\en-ner-location.bin");
			models.add(locationStream);
			orgStream = new FileInputStream(".\\models\\opennlp\\en-ner-organization.bin");
			models.add(orgStream);
			timeStream = new FileInputStream(".\\models\\opennlp\\en-ner-time.bin");
			models.add(timeStream);


			// NER Models
			TokenNameFinderModel personModel = new TokenNameFinderModel(personStream);
			TokenNameFinderModel dateModel = new TokenNameFinderModel(dateStream);
			TokenNameFinderModel locationModel  = new TokenNameFinderModel(locationStream);
			TokenNameFinderModel orgModel  = new TokenNameFinderModel(orgStream);
			TokenNameFinderModel timeModel  = new TokenNameFinderModel(timeStream);

			//finders
			NameFinderME personFinder = new NameFinderME(personModel);
			NameFinderME dateFinder = new NameFinderME(dateModel);
			NameFinderME locationFinder = new NameFinderME(locationModel);
			NameFinderME orgFinder = new NameFinderME(orgModel);
			NameFinderME timeFinder = new NameFinderME(timeModel);

			//find NEs
			List<String> pNE = new ArrayList<String>();
			List<String> dNE = new ArrayList<String>();
			List<String> lNE = new ArrayList<String>();
			List<String> oNE = new ArrayList<String>();
			List<String> tNE = new ArrayList<String>();
			for(String query : sentences){

				//tokenize
				String[] tokens = tokenizeSentence(query);

				//find and store
				Span personNames[] = personFinder.find(tokens);
				pNE.addAll(Arrays.asList(Span.spansToStrings(personNames, tokens)));
				
				Span dateNames[] = dateFinder.find(tokens);
				dNE.addAll(Arrays.asList(Span.spansToStrings(dateNames, tokens)));
				
				Span locationNames[] = locationFinder.find(tokens);
				lNE.addAll(Arrays.asList(Span.spansToStrings(locationNames, tokens)));
				
				Span orgNames[] = orgFinder.find(tokens);
				oNE.addAll(Arrays.asList(Span.spansToStrings(orgNames, tokens)));
				
				Span timeNames[] = timeFinder.find(tokens);
				tNE.addAll(Arrays.asList(Span.spansToStrings(timeNames, tokens)));

			}
			
			//store NEs
			multimap.put("person", pNE);
			multimap.put("date", dNE);
			multimap.put("location", lNE);
			multimap.put("organization", oNE);
			multimap.put("time", tNE);
			
			//close the handles
			dateStream.close();
			personStream.close();
			locationStream.close();
			orgStream.close();
			timeStream.close();
		}
		catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		return multimap;
	}
}

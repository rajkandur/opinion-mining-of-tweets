package parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.map.MultiValueMap;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class StanfordWrapParser {

	//LexicalizedParser m_lexicalparser;
	//TokenizerFactory<CoreLabel> m_tokenizerFactory;
	AbstractSequenceClassifier<CoreLabel> m_NEclassifier;
	MaxentTagger m_POStagger;
	static StanfordWrapParser m_StanfordParser;

	public StanfordWrapParser() {
		m_StanfordParser =  null;
		//m_lexicalparser = new LexicalizedParser("./models/stanfordnlp/englishPCFG.ser.gz");
		//m_tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
		m_NEclassifier = CRFClassifier.getClassifierNoExceptions("models/stanfordnlp/classifiers/english.muc.7class.distsim.crf.ser.gz");
		m_POStagger = new MaxentTagger("models/stanfordnlp/pos/english-bidirectional-distsim.tagger");
	}

	public static StanfordWrapParser getInstance(){
		if( null == m_StanfordParser)
			m_StanfordParser = new StanfordWrapParser();	
		return m_StanfordParser;
	}

	//	String[] parseSentence(String s) {
	//
	//		//courtesy: stanford examples
	//
	//		//tokenizing the sentence
	//		List<CoreLabel> words = m_tokenizerFactory.getTokenizer(new StringReader(s)).tokenize();
	//		Tree parse = m_lexicalparser.apply(words);
	//
	//		//parse tree print
	//		TreePrint treeprint = new TreePrint("penn,typedDependenciesCollapsed");
	//		treeprint.printTree(parse); //printing on the console
	//
	//		//convert tree to string
	//		String[] parseStr =  new String[1];
	//		parseStr[0] = new String();
	//
	//		OutputStream os = new ByteArrayOutputStream();
	//		PrintWriter pw = new PrintWriter(os);
	//
	//		treeprint.printTree(parse, pw);
	//		parseStr[0] = os.toString();
	//
	//		return parseStr;
	//	}

	public MultiValueMap findNamedEntities(String s)  {

		String annotatedNEs = m_NEclassifier.classifyWithInlineXML(s);

		//regex
		String orgregexpat = "(<ORGANIZATION>([a-zA-Z0-9 ]+)</ORGANIZATION>)";
		String perregexpat = "(<PERSON>([a-zA-Z0-9 ]+)</PERSON>)";
		String locregexpat = "(<LOCATION>([a-zA-Z0-9 ]+)</LOCATION>)";
		String datregexpat = "(<DATE>([a-zA-Z0-9 ]+)</DATE>)";
		String timregexpat = "(<TIME>([a-zA-Z0-9 ]+)</TIME>)";
		String monregexpat = "(<MONEY>([a-zA-Z0-9 ]+)</MONEY>)";

		//create a pattern object from the regex
		Pattern orgpattern = Pattern.compile(orgregexpat);
		Pattern perpattern = Pattern.compile(perregexpat);
		Pattern locpattern = Pattern.compile(locregexpat);
		Pattern datpattern = Pattern.compile(datregexpat);
		Pattern timpattern = Pattern.compile(timregexpat);
		Pattern monpattern = Pattern.compile(monregexpat);

		//get the matched pattern if present
		Matcher matcher = orgpattern.matcher(annotatedNEs);
		List<String> oNE = new ArrayList<String>();
		while(matcher.find())
			oNE.add(matcher.group(2));
		
		matcher = perpattern.matcher(annotatedNEs);
		List<String> pNE = new ArrayList<String>();
		while(matcher.find())
			pNE.add(matcher.group(2));
		
		matcher = locpattern.matcher(annotatedNEs);
		List<String> lNE = new ArrayList<String>();
		while(matcher.find())
			lNE.add(matcher.group(2));
		
		matcher = datpattern.matcher(annotatedNEs);
		List<String> dNE = new ArrayList<String>();
		while(matcher.find())
			dNE.add(matcher.group(2));
		
		matcher = timpattern.matcher(annotatedNEs);
		List<String> tNE = new ArrayList<String>();
		while(matcher.find())
			tNE.add(matcher.group(2));
		
		matcher = monpattern.matcher(annotatedNEs);
		List<String> mNE = new ArrayList<String>();
		while(matcher.find())
			mNE.add(matcher.group(2));		
		
		//store it
		MultiValueMap multiMap = new MultiValueMap();
		multiMap.put("ORG", oNE);
		multiMap.put("PER", pNE);
		multiMap.put("LOC", lNE);
		multiMap.put("DAT", dNE);
		multiMap.put("TIM", tNE);
		multiMap.put("MON", mNE);

		return multiMap;
	}

	public List<String> tagSentence(String sen){
		List<List<HasWord>> sentences = MaxentTagger.tokenizeText(new StringReader(sen));
		ArrayList<TaggedWord> tSentence = null;
	    for (List<HasWord> sentence : sentences) {
	      tSentence = m_POStagger.tagSentence(sentence);
	      //System.out.println(Sentence.listToString(tSentence, false));
	    }
	    
	    //return only tags
	    List<String> tags = new ArrayList<String>();
	    for (TaggedWord tagwd : tSentence) {
			tags.add(tagwd.tag());
		}
	    
	    return tags;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StanfordWrapParser parser = new StanfordWrapParser();
		List<String> tags = parser.tagSentence("#suckit @obamawin http://t.co.dfdfdf :-)");
		System.out.println(tags);
	}

}

